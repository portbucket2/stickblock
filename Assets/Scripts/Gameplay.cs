﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        UIManager.Instance.LoadLevelNumber();
       
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
       // LoadNextLevel();
    }
    IEnumerator StartGameRoutine()
    {
        yield return new WaitForEndOfFrame();
        UIManager.Instance.LoadLevelNumber();
    }

    public void LevelComplete()
    {
        LevelManager.Instance.IncreaseGameLevel();
        UIManager.Instance.ShowLevelComplete();
    }
    public void LoadNextLevel()
    {
        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        SceneManager.LoadScene("Level"+t_CurrentLevel);
    }
}
