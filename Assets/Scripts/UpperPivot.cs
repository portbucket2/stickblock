﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UpperPivot : MonoBehaviour
{
    public UnityEvent OnTriggerWithPlatform;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("platform"))
        {
            OnTriggerWithPlatform.Invoke();
        }
    }
}
