﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStickBlock : MonoBehaviour
{
    public static PlayerStickBlock Instance;

    public Vector3 initPos;
    public Quaternion initRot;


    public bool isSwipeLeft;
    public bool isSwipeRight;
    public bool isSwipeUp;
    public bool isSwipeDown;
    public bool isWrongMove;


    [Range(0f, 10f)] public float moveSpeed;

    public bool isMoveCubeUp;
    public bool isMoveCubeDown;

    public bool isMoveStickUp;
    public bool isMoveStickDown;

    public bool isLeftOpen;
    public bool isRightOpen;

    


    public Transform upperTrans;
    public Transform lowerTrans;


    public GameObject cubePlayer;
    public GameObject stickObject;

    public Transform upperPivot;
    public Transform lowerPivot;

    public StickState stickState;
    public StickPosState stickPosState;
    public CubeDirectionState cubeDirState;
    public CubePosState cubePosState;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Update()
    {
        if (isMoveCubeUp)
        {
            cubePlayer.transform.position = Vector3.MoveTowards(cubePlayer.transform.position, upperTrans.position, moveSpeed * Time.deltaTime);

            if (Vector3.Distance(cubePlayer.transform.position, upperTrans.position) < 0.001f)
            {
                cubePlayer.transform.position = upperTrans.position;
                cubePosState = CubePosState.UPPER;
                stickObject.transform.SetParent(cubePlayer.transform);
                isMoveCubeUp = false;
            }
        }
        if (isMoveCubeDown)
        {
            cubePlayer.transform.position = Vector3.MoveTowards(cubePlayer.transform.position, lowerTrans.position, moveSpeed * Time.deltaTime);
            if (Vector3.Distance(cubePlayer.transform.position, lowerTrans.position) < 0.001f)
            {
                cubePlayer.transform.position = lowerTrans.position;
                cubePosState = CubePosState.DOWN;
                stickObject.transform.SetParent(cubePlayer.transform);
                isMoveCubeDown = false;
            }
        }
        if (isMoveStickUp)
        {
            upperTrans.transform.position = Vector3.MoveTowards(upperTrans.transform.position, cubePlayer.transform.position, moveSpeed * Time.deltaTime);

            if (Vector3.Distance(upperTrans.transform.position, cubePlayer.transform.position) < 0.001f)
            {
                upperTrans.transform.position = cubePlayer.transform.position;
                stickPosState = StickPosState.UPPER;
                stickObject.transform.SetParent(transform);
                upperTrans.SetParent(stickObject.transform);
                stickObject.transform.SetParent(cubePlayer.transform);
                cubePosState = CubePosState.UPPER;
                isMoveStickUp = false;
                CheckIfLeftRightOpen();
            }
        }
        if (isMoveStickDown)
        {
            lowerTrans.transform.position = Vector3.MoveTowards(lowerTrans.transform.position, cubePlayer.transform.position, moveSpeed * Time.deltaTime);

            if (Vector3.Distance(lowerTrans.transform.position, cubePlayer.transform.position) < 0.001f)
            {
                lowerTrans.transform.position = cubePlayer.transform.position;
                stickPosState = StickPosState.DOWN;
                stickObject.transform.SetParent(transform);
                lowerTrans.SetParent(stickObject.transform);
                stickObject.transform.SetParent(cubePlayer.transform);
                cubePosState = CubePosState.DOWN;
                isMoveStickDown = false;
                CheckIfLeftRightOpen();
            }
        }

    }

    public void CheckIfLeftRightOpen()
    {
        if (stickState == StickState.HORIZONTAL && cubePosState == CubePosState.DOWN && isSwipeLeft)
        {
            isRightOpen = true;
            isLeftOpen = false;
            Debug.Log("called here hori down");
        }
        else if (stickState == StickState.HORIZONTAL && cubePosState == CubePosState.UPPER)
        {
            isRightOpen = false;
            isLeftOpen = true;
            Debug.Log("called here hori up");
        }
        /*else if(stickState == StickState.VERTICAL && cubePosState == CubePosState.DOWN)
        {
            isRightOpen = true;
            isLeftOpen = false;
        }
        else if (stickState == StickState.VERTICAL && cubePosState == CubePosState.UPPER)
        {
            isRightOpen = false;
            isLeftOpen = true;
        }*/
    }


    private StickState CheckStickState()
    {
        if (stickState == StickState.VERTICAL)
        {
            return StickState.HORIZONTAL;
        }
        else
        {
            return StickState.VERTICAL;
        }
    }
    private CubeDirectionState CheckCubeDirectionState()
    {
        if (cubeDirState == CubeDirectionState.UP && isSwipeLeft)
        {
            return CubeDirectionState.LEFT;
        }
        else if (cubeDirState == CubeDirectionState.LEFT && (isSwipeRight || isSwipeLeft))
        {
            return CubeDirectionState.UP;
        }
        else if (cubeDirState == CubeDirectionState.UP && isSwipeRight)
        {
            return CubeDirectionState.RIGHT;
        }
        else if (cubeDirState == CubeDirectionState.RIGHT && isSwipeLeft)
        {
            return CubeDirectionState.UP;
        }
        else
        {
            return CubeDirectionState.NONE;
        }
    }
    public void SwipeReset()
    {
        isSwipeLeft = false;
        isSwipeRight = false;
        isSwipeDown = false;
        isSwipeUp = false;
    }

    public void MoveDirection(Swipe swipe)
    {


        switch (swipe)
        {
            case Swipe.None:
                break;
            case Swipe.Up:
                SwipeReset();
                isSwipeUp = true;
                MoveUp();
                break;
            case Swipe.Down:
                SwipeReset();
                isSwipeDown = true;
                MoveDown();
                break;
            case Swipe.Left:
                initPos = cubePlayer.transform.position;
                initRot = cubePlayer.transform.rotation;

                if (stickState == StickState.VERTICAL && cubePosState == CubePosState.UPPER && isGrounded == false)
                {
                    SwipeReset();
                    isSwipeLeft = true;
                    

                    cubeDirState = CheckCubeDirectionState();
                    stickState = CheckStickState();
                    MoveLeftBasedPivot();
                }
                else
                {
                    if (stickState == StickState.HORIZONTAL)
                    {
                       // if (isLeftOpen)
                        {
                            SwipeReset();
                            isSwipeLeft = true;
                            

                            cubeDirState = CheckCubeDirectionState();
                            stickState = CheckStickState();
                            MoveLeft();
                        }
                    }
                    else
                    {
                        isSwipeLeft = true;
                        isSwipeRight = false;

                        cubeDirState = CheckCubeDirectionState();
                        stickState = CheckStickState();
                        MoveLeft();
                    }
                }
                break;
            case Swipe.Right:
                isSwipeLeft = false;
                isSwipeRight = true;

                initPos = cubePlayer.transform.position;
                initRot = cubePlayer.transform.rotation;

                MoveRight();
                break;

            default:
                break;
        }

        CheckIfLeftRightOpen();
    }

    private void MoveLeftBasedPivot()
    {
        lowerPivot.SetParent(transform);
        cubePlayer.transform.SetParent(lowerPivot);
        StartCoroutine(XPivotNegmoveRoutine());
    }

    private IEnumerator XPivotNegmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.x;
        float t_DestValue = t_CurrentFillValue - 1;

        float t_CurrentRot = lowerPivot.transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            //cubePlayer.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), cubePlayer.transform.position.y, cubePlayer.transform.position.z);
            lowerPivot.transform.rotation = Quaternion.Euler(lowerPivot.transform.rotation.x, lowerPivot.transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                cubePlayer.transform.SetParent(transform);
                lowerPivot.SetParent(stickObject.transform);

                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(XNegmoveRoutine());
    }

    public void MoveCube()
    {

        if (stickState == StickState.VERTICAL)
        {
            switch (cubePosState)
            {
                case CubePosState.NONE:
                    break;
                case CubePosState.UPPER:
                    stickObject.transform.SetParent(transform);
                    isMoveCubeDown = true;
                    break;
                case CubePosState.DOWN:
                    stickObject.transform.SetParent(transform);
                    isMoveCubeUp = true;
                    break;
                default:
                    break;
            }
        }
        else if (stickState == StickState.HORIZONTAL)
        {
            switch (stickPosState)
            {
                case StickPosState.NONE:
                    break;
                case StickPosState.UPPER:
                    stickObject.transform.SetParent(transform);
                    lowerTrans.SetParent(transform);
                    stickObject.transform.SetParent(lowerTrans);
                    isMoveStickDown = true;

                    break;
                case StickPosState.DOWN:
                    stickObject.transform.SetParent(transform);
                    upperTrans.SetParent(transform);
                    stickObject.transform.SetParent(upperTrans);
                    isMoveStickUp = true;

                    break;
                default:
                    break;
            }
        }

    }

    private void MoveLeft()
    {
        StartCoroutine(XNegmoveRoutine());
    }
    private void MoveRight()
    {
        StartCoroutine(XPosmoveRoutine());
    }
    private void MoveUp()
    {
        before = cubePlayer.transform.eulerAngles;
        StartCoroutine(ZPosmoveRoutine());
    }
    private void MoveDown()
    {
        before = cubePlayer.transform.eulerAngles;
        StartCoroutine(ZNegmoveRoutine());
    }

    Vector3 before;

    IEnumerator ZPosmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.z;
        float t_DestValue = t_CurrentFillValue + 1;

        float t_CurrentRot = cubePlayer.transform.rotation.eulerAngles.x;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cubePlayer.transform.position = new Vector3(cubePlayer.transform.position.x, cubePlayer.transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));
            cubePlayer.transform.rotation = Quaternion.Euler(Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression),cubePlayer.transform.rotation.eulerAngles.y, cubePlayer.transform.rotation.eulerAngles.z);

            if (t_Progression >= 1f)
            {
                CheckValidity();
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(ZPosmoveRoutine());
    }
    IEnumerator ZNegmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.z;
        float t_DestValue = t_CurrentFillValue - 1;

        float t_CurrentRot = cubePlayer.transform.rotation.eulerAngles.x;
        float t_DestRot = t_CurrentRot - 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cubePlayer.transform.position = new Vector3(cubePlayer.transform.position.x, cubePlayer.transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));
            
            cubePlayer.transform.rotation = Quaternion.Euler(Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression), cubePlayer.transform.rotation.eulerAngles.y, cubePlayer.transform.rotation.eulerAngles.z);
           


            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }
        CheckValidity();
        StopCoroutine(ZNegmoveRoutine());
    }
    IEnumerator XPosmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.x;
        float t_DestValue = t_CurrentFillValue + 1;

        float t_CurrentRot = cubePlayer.transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot - 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cubePlayer.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), cubePlayer.transform.position.y, cubePlayer.transform.position.z);
            cubePlayer.transform.rotation = Quaternion.Euler(cubePlayer.transform.rotation.x, cubePlayer.transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                CheckValidity();
                break;
            }

            yield return t_CycleDelay;
        }
        if (!isWrongMove)
        {
            cubeDirState = CheckCubeDirectionState();
            stickState = CheckStickState();
        }
        StopCoroutine(XPosmoveRoutine());
    }

    IEnumerator XNegmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.x;
        float t_DestValue = t_CurrentFillValue - 1;

        float t_CurrentRot = cubePlayer.transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cubePlayer.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), cubePlayer.transform.position.y, cubePlayer.transform.position.z);
            cubePlayer.transform.rotation = Quaternion.Euler(cubePlayer.transform.rotation.x, cubePlayer.transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                CheckValidity();
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(XNegmoveRoutine());
    }

    public void TriggeredwithGround()
    {
        isGrounded = true;
    }
    public void PlayerExitFromGround()
    {
        isGrounded = false;
    }
    public bool isGrounded;




    public void CheckValidity()
    {
        if (isGrounded) return;


        if(isGrounded == false && isSwipeRight)
        {
            isWrongMove = true;
            StartCoroutine(XPosReversemoveRoutine());
        }
        else if(isGrounded == false && isSwipeLeft)
        {
            isWrongMove = true;
            StartCoroutine(XNegReversemoveRoutine());
        }
        else if(isGrounded == false && isSwipeDown)
        {
            isWrongMove = true;
            StartCoroutine(ZNegReversemoveRoutine());
        }
        else if(isGrounded == false && isSwipeUp)
        {
            isWrongMove = true;
            StartCoroutine(ZPosReversemoveRoutine());
        }
    }

    IEnumerator XPosReversemoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.x;
        float t_DestValue = t_CurrentFillValue - 1;

        float t_CurrentRot = cubePlayer.transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cubePlayer.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), cubePlayer.transform.position.y, cubePlayer.transform.position.z);
            cubePlayer.transform.rotation = Quaternion.Euler(cubePlayer.transform.rotation.x, cubePlayer.transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                isWrongMove = false;
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(XPosReversemoveRoutine());
    }

    IEnumerator XNegReversemoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.x;
        float t_DestValue = t_CurrentFillValue + 1;

        float t_CurrentRot = cubePlayer.transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot - 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cubePlayer.transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), cubePlayer.transform.position.y, cubePlayer.transform.position.z);
            cubePlayer.transform.rotation = Quaternion.Euler(cubePlayer.transform.rotation.x, cubePlayer.transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                isWrongMove = false;
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(XNegReversemoveRoutine());
    }
    public float Clamp0360(float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if (result < 0)
        {
            result += 360f;
        }
        return result;
    }
    IEnumerator ZNegReversemoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.z;
        float t_DestValue = t_CurrentFillValue + 1;

        float t_CurrentRot = cubePlayer.transform.rotation.eulerAngles.x;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();


        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cubePlayer.transform.position = new Vector3(cubePlayer.transform.position.x, cubePlayer.transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));

           // cubePlayer.transform.rotation = Quaternion.Euler(Vector3.Slerp(rot,before,t_Progression));
            cubePlayer.transform.Rotate(Vector3.right,90*Time.deltaTime/t_Duration,Space.World);

           
            if (t_Progression >= 1f)
            {
                isWrongMove = false;
                break;
            }

            yield return t_CycleDelay;
        }

        cubePlayer.transform.eulerAngles = before;

        StopCoroutine(ZNegReversemoveRoutine());
    }
    IEnumerator ZPosReversemoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = cubePlayer.transform.position.z;
        float t_DestValue = t_CurrentFillValue - 1;

        float t_CurrentRot = cubePlayer.transform.rotation.eulerAngles.x;
        float t_DestRot = t_CurrentRot - 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cubePlayer.transform.position = new Vector3(cubePlayer.transform.position.x, cubePlayer.transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));
            //cubePlayer.transform.rotation = Quaternion.Euler(Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression), cubePlayer.transform.rotation.eulerAngles.y, cubePlayer.transform.rotation.eulerAngles.z);
            cubePlayer.transform.Rotate(Vector3.right, -90 * Time.deltaTime / t_Duration, Space.World);
            if (t_Progression >= 1f)
            {
                isWrongMove = false;
                break;
            }

            yield return t_CycleDelay;
        }

        cubePlayer.transform.eulerAngles = before;
        StopCoroutine(ZPosReversemoveRoutine());
    }

    public void TriggerWithPivotUpper()
    {
        //if (stickState == StickState.HORIZONTAL)
        //{
        //    Debug.LogWarning(" WArning ");
        //    StopAllCoroutines();
        //    cubePlayer.transform.position = initPos;
        //    cubePlayer.transform.rotation = initRot;
        //}
    }
    public void TriggerWithPivotDown()
    {
        //if (stickState == StickState.HORIZONTAL)
        //{
        //    Debug.LogWarning(" WArning ");
        //    StopAllCoroutines();
        //    cubePlayer.transform.position = initPos;
        //    cubePlayer.transform.rotation = initRot;
        //}
    }

}
