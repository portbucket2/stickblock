﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public bool isGrounded;
    public bool isSwipeLeft;
    public bool isSwipeRight;
    public bool isSwipeUp;
    public bool isSwipeDown;
    public bool isWrongMove;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    
    }

    public void MoveDirection(Swipe swipe)
    {
        switch (swipe)
        {
            case Swipe.None:
                break;
            case Swipe.Up:
                SwipeReset();
                isSwipeUp = true;

                MoveUp();
                break;
            case Swipe.Down:
                SwipeReset();
                isSwipeDown = true;
                MoveDown();
                break;
            case Swipe.Left:
                SwipeReset();
                isSwipeLeft = true;
                MoveLeft();
                break;
            case Swipe.Right:
                SwipeReset();
                isSwipeRight = true;
                MoveRight();
                break;
           
            default:
                break;
        }
    }

    private void MoveLeft()
    {
        StartCoroutine(XNegmoveRoutine());
    }
    private void MoveRight()
    {
        StartCoroutine(XPosmoveRoutine());
    }
    private void MoveUp()
    {
        StartCoroutine(ZPosmoveRoutine());
    }
    private void MoveDown()
    {
        StartCoroutine(ZNegmoveRoutine());
    }


    IEnumerator XNegmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.x;
        float t_DestValue = t_CurrentFillValue-1;

        float t_CurrentRot = transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), transform.position.y,transform.position.z);
            transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                CheckValidity();
                break;
            }

            yield return t_CycleDelay;
        }
        
        StopCoroutine(XNegmoveRoutine());
    }
    IEnumerator XPosmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.x;
        float t_DestValue = t_CurrentFillValue + 1;

        float t_CurrentRot = transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot - 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), transform.position.y, transform.position.z);
            transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                CheckValidity();
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(XPosmoveRoutine());
    }

    IEnumerator ZPosmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.z;
        float t_DestValue = t_CurrentFillValue + 1;

        float t_CurrentRot = transform.rotation.eulerAngles.x;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));
            transform.rotation = Quaternion.Euler(Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression),transform.rotation.y, transform.rotation.z);

            if (t_Progression >= 1f)
            {
                CheckValidity();
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(ZPosmoveRoutine());
    }
    IEnumerator ZNegmoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.z;
        float t_DestValue = t_CurrentFillValue - 1;

        float t_CurrentRot = transform.rotation.eulerAngles.x;
        float t_DestRot = t_CurrentRot - 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));
            transform.rotation = Quaternion.Euler(Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression), transform.rotation.y, transform.rotation.z);

            if (t_Progression >= 1f)
            {
                CheckValidity();
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(ZNegmoveRoutine());
    }

    public void SwipeReset()
    {
        isSwipeLeft = false;
        isSwipeRight = false;
        isSwipeDown = false;
        isSwipeUp = false;
    }
    public void CheckValidity()
    {
        if (isGrounded) return;


        if (isGrounded == false && isSwipeRight)
        {
            isWrongMove = true;
            StartCoroutine(XPosReversemoveRoutine());
        }
        else if (isGrounded == false && isSwipeLeft)
        {
            isWrongMove = true;
            StartCoroutine(XNegReversemoveRoutine());
        }
        else if (isGrounded == false && isSwipeDown)
        {
            isWrongMove = true;
            StartCoroutine(ZNegReversemoveRoutine());
        }
        else if (isGrounded == false && isSwipeUp)
        {
            isWrongMove = true;
            StartCoroutine(ZPosReversemoveRoutine());
        }
    }
    IEnumerator ZNegReversemoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.z;
        float t_DestValue = t_CurrentFillValue + 1;

        float t_CurrentRot = transform.rotation.eulerAngles.x;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));
            transform.rotation = Quaternion.Euler(Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression), transform.rotation.y, transform.rotation.z);

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(ZNegReversemoveRoutine());
    }
    IEnumerator ZPosReversemoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.z;
        float t_DestValue = t_CurrentFillValue - 1;

        float t_CurrentRot = transform.rotation.eulerAngles.x;
        float t_DestRot = t_CurrentRot - 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));
            transform.rotation = Quaternion.Euler(Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression), transform.rotation.y, transform.rotation.z);

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(ZPosReversemoveRoutine());
    }
    IEnumerator XPosReversemoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.x;
        float t_DestValue = t_CurrentFillValue - 1;

        float t_CurrentRot = transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot + 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), transform.position.y, transform.position.z);
            transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(XPosReversemoveRoutine());
    }
    IEnumerator XNegReversemoveRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.4f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = transform.position.x;
        float t_DestValue = t_CurrentFillValue + 1;

        float t_CurrentRot = transform.rotation.eulerAngles.z;
        float t_DestRot = t_CurrentRot - 90;

        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), transform.position.y, transform.position.z);
            transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, Mathf.Lerp(t_CurrentRot, t_DestRot, t_Progression));

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }
        StopCoroutine(XNegReversemoveRoutine());
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("platform"))
        {
            isGrounded = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("platform"))
        {
            isGrounded = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("platform"))
        {
            isGrounded = false;
        }
    }

}
