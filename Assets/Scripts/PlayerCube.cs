﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerCube : MonoBehaviour
{
    public UnityEvent OnPlayerGrounded;
    public UnityEvent OnPlayerNotGrounded;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("platform"))
        {
            OnPlayerGrounded.Invoke();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("platform"))
        {
            OnPlayerGrounded.Invoke();
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("platform"))
        {
            
            OnPlayerNotGrounded.Invoke();
        }
    }
}
