﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class FinalDestCube : MonoBehaviour
{
    public UnityEvent OnPlayerReachDestCube;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggered "+other.name);
        if (other.CompareTag("Player"))
        {
            StartCoroutine(WaitAndSend());
        }
    }

    IEnumerator WaitAndSend()
    {
        yield return new WaitForSeconds(0.25f);
        OnPlayerReachDestCube.Invoke();
    }
}
