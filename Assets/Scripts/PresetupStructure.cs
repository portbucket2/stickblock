﻿public enum StickState
{
    NONE,
    VERTICAL,
    HORIZONTAL
}

public enum CubeDirectionState
{
    NONE, 
    LEFT,
    UP,
    RIGHT
}
public enum CubePosState
{
    NONE,
    UPPER,
    DOWN
}

public enum StickPosState
{
    NONE,
    UPPER,
    DOWN
}